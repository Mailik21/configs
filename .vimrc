"NeoBundle Scripts-----------------------------
"
if has('vim_starting')
  if &compatible
    set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
"
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
"NeoBundle 'Shougo/neosnippet.vim'
"NeoBundle 'Shougo/neosnippet-snippets'
"NeoBundle 'tpope/vim-fugitive'
NeoBundle 'SirVer/ultisnips'
NeoBundle 'honza/vim-snippets'
"NeoBundle 'mattn/emmet-vim'
NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-repeat'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'vim-php/tagbar-phpctags.vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'scrooloose/nerdcommenter'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'majutsushi/tagbar'              " Class/module browser
NeoBundle 'StanAngeloff/php.vim'            "Более полная подсветка синтаксиса
" You can specify revision/branch/tag.
"NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }
NeoBundle 'Shougo/neocomplete'
"Interactive command execution in Vim
NeoBundle 'Shougo/vimproc.vim', {
\ 'build' : {
\     'windows' : 'tools\\update-dll-mingw',
\     'cygwin' : 'make -f make_cygwin.mak',
\     'mac' : 'make -f make_mac.mak',
\     'linux' : 'make',
\     'unix' : 'gmake',
\    },
\ }
"Search and display information from arbitary sources like files
"NeoBundle 'Shougo/unite.vim'

"NeoBundle 'm2mdas/phpcomplete-extended'
NeoBundle 'bling/vim-airline'


" Required:
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
"End NeoBundle Scripts-------------------------


"Окружение
    set laststatus=2
    set syntax=on
    "Авто-перезагрузка .vimr cпосле сохранения на лету
    if has('autocmd')
        autocmd! bufwritepost .vimrc source $MYVIMRC
    endif
    syn sync minlines=9999
    set backspace=indent,eol,start



"Интерфейс
    colorscheme 256-jungle
    "кодировка внутри vim
    set encoding=utf-8
    "кодировка при редактировании файлов
    set fileencodings=utf-8,cp1251
    "всегда добавлять lf в конце файла
    set fileformat=unix
    "доступные варианты дополнения команд над командной строкой
    set wildmenu
    "Когда больше одного совпадения, показать список вариантов и дополнить
    "первое совпадение. Затем завершуть следующего полное совпадение
    set wildmode=longest:list,full
    "Игнор-список при автодополнении файлов/папок
    set wildignore+=.git,.svn,.hg
    set title
    "Авто-дополнение незавершенных команд в строке ввода команд
    set showcmd
    "Показывать парную скобку
    set showmatch
    "Чтобы курсор при скроллинге всегда был в середине окна
    "set scrolloff=999
    "Всегда показывать вкладки
    set showtabline=2
    "Показывать невидимые символы
    set list
    set listchars=tab:▸\ ,trail:·,extends:❯,precedes:❮,nbsp:×

    "Ширина поля нумерации строк
    set numberwidth=4
    "Номера строк
    set number
    set wrap
    set linebreak
    set linebreak
    if has("linebreak")
        let &sbr = nr2char(8618).' '
    endif

    "Номер колонки, которая будет подсвечена
    "set colorcolumn=80
    "Ширина текста, более длинные строки обрезаются по последнему пробелу
    "set textwidth=80
    "Скопировать отступ текущей строки при добавлении новой строки
    set autoindent
    "Умные отступы
    set smartindent
    "Количество пробелов для каждого шага (auto)indent
    set shiftwidth=4
    "Использовать пробелы вместо tab
    set expandtab
    "Количество пробелов, которые содержит tab
    set tabstop=4
    "--//-- при выполнении операций редактирования
    set softtabstop=4
    set linespace=1
    "Подсветить линию с курсором
    "set cursorline
    "Количество цветов
    set t_Co=256
    "Не показывать "вступление" при запуске
    set shortmess+=I

    "Ни бипов, ни всплывающих вещей
    set visualbell
    set t_vb=
    "Список директории, в которыех будет поиск при выполнении команд :find, gf
    set path=.,,**
    "Перечитывать измененные файлы автоматически
    set autoread

"Вкладки
    set foldmethod=indent
    set foldnestmax=10
    set nofoldenable
    set foldlevel=1
    set fillchars="fold: "

"Поиск
    "Останавливать поиск по достижению конца файла
    set nowrapscan
    "Поиск фрагмента текста во время набора
    set incsearch
    "Подсветка результатов поиска
    set hlsearch
    "Игнорировать регистр символов при поиске
    set ignorecase
    set smartcase
    "включаем флаг глобальный по-умолчанию для регулярок
    set gdefault

""Шорткаты
    let mapleader = ","
    set pastetoggle=<leader>p
    nnoremap <leader>s :<C-u>%s//<left>
    vnoremap <leader>s :s//<left>
    inoremap <leader>d <C-k>


    "Отключаем стрелки
        inoremap <Up> <NOP>
        inoremap <Down> <NOP>
        inoremap <Left> <NOP>
        inoremap <Right> <NOP>
        noremap <Up> <NOP>
        noremap <Down> <NOP>
        noremap <Left> <NOP>
        noremap <Right> <NOP>
        "Навигация в Режим вставки с <Ctrl>-hjkl
        inoremap <C-h> <C-o>h
        inoremap <C-j> <C-o>j
        inoremap <C-k> <C-o>k
        inoremap <C-l> <C-o>l

    "Переключанием между экранами "nnoremap <C-h> <C-W>h
    nnoremap <C-j> <C-W>j
    nnoremap <C-k> <C-W>k
    nnoremap <C-l> <C-W>l

    ",nm
    let g:relativenumber = 0
    function! ToogleRelativeNumber()
        if g:relativenumber == 0
            let g:relativenumber = 1
            set norelativenumber
            set number
            echo "Show line number"
        elseif g:relativenumber == 1
            let g:relativenumber = 2
            set nonumber
            set relativenumber
            echo "Show relative line numbers"
        else
            let g:relativenumber = 0
            set nonumber
            set norelativenumber
            echo "Show no line numbers"
        endif
    endfunction
    nnoremap <leader>nm :<C-u>call ToogleRelativeNumber()<CR>

    "<Esc><Esc>
   " nnoremap <silent> <Esc><Esc> :nohlsearch<CR><Esc>

    "n и N
    "Результаты поиска всегда по центру экрана
    nnoremap n nzz
    nnoremap N Nzz
    nnoremap * *zz
    nnoremap # #zz
    nnoremap g* g*zz
    nnoremap g# g#zz

    ",v
    "Открыть .vimrc в новой вкладке
    nnoremap <leader>v :<C-u>tabedit $MYVIMRC<CR>

    "Ctrl+s
    nmap <F2> :w<CR>
    imap <F2> <ESC>:w<CR>

    noremap <A-j> :gT <CR> "Переход к предыдущей вкладке
    noremap <A-k> :gt <CR> "Переход к следующей вкладке
    nmap <F9> :TagbarToggle<CR>
    nmap <F8> :NERDTreeToggle<CR>
    nmap <C-\> :NERDTreeFind<CR>

    let g:UltiSnipsExpandTrigger="<tab>"
    let g:UltiSnipsEditSplit="vertical"

"Плагины
    "Begin NeoComplete plugin settings ------------
        "Disable AutoComplPop
        let g:acp_enableAtStartup = 0
        "Use neocomplete
        let g:neocomplete#enable_at_startup = 1
        "Use smartcase
        let g:neocomplete#enable_smart_case = 1
        "Set minimum syntax keyword length
        let g:neocomplete#sources#syntax#min_keyword_length = 3
        let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

        "Plugin key-mappings
        inoremap <expr><C-g> neocomplete#undo_completion()
        inoremap <expr><C-l> neocomplete#complete_common_string()

        "Recommended key-mappings
        "<CR>: close popup and save indent
        inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
        function! s:my_cr_function()
          return neocomplete#close_popup() . "\<CR>"
          " For no inserting <CR> key.
          "return pumvisible() ? neocomplete#close_popup() : "\<CR>"
        endfunction
        " <TAB>: completion.
        inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
        " <C-h>, <BS>: close popup and delete backword char.
        inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
        inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
        inoremap <expr><C-y>  neocomplete#close_popup()
        inoremap <expr><C-e>  neocomplete#cancel_popup()
        " Close popup by <Space>.
        "inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

        " For cursor moving in insert mode(Not recommended)
        "inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
        "inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
        "inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
        "inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
        " Or set this.
        "let g:neocomplete#enable_cursor_hold_i = 1
        " Or set this.
        "let g:neocomplete#enable_insert_char_pre = 1

        " AutoComplPop like behavior.
        "let g:neocomplete#enable_auto_select = 1

        " Shell like behavior(not recommended).
        "set completeopt+=longest
        "let g:neocomplete#enable_auto_select = 1
        "let g:neocomplete#disable_auto_complete = 1
        "inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

        " Enable omni completion.
        autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
        autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
        autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
        autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
        autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
        autocmd FileType php setlocal omnifunc=phpcomplete_extended#CompletePHP

        " Enable heavy omni completion.
        let g:neocomplete#sources#omni#input_patterns = {}
        "let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
        "let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
        "let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

        " For perlomni.vim setting.
        " https://github.com/c9s/perlomni.vim
        let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
        "End NeoComplete plugin settings ---------------

    "Tagbar
        let g:tagbar_phpctags_bin = '~/.vim/bin/phpctags'
        let g:tagbar_phpctags_memory_limit = '256M'
        let g:tagbar_autofocus = 1
        let g:tagbar_expand = 1

    "Vim-airline
        let g:airline_powerline_fonts=1

"let g:php_syntax_extensions_enabled = 1
